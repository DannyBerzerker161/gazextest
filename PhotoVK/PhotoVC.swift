

import UIKit

class FotoVC: UIViewController {
    
    var image : UIImage?

    var lat : String = ""
    var long : String = ""
    
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.image = image
        print("error1")
        if lat != "" && long != "" {
            print("error2")
            let rightBut = UIBarButtonItem(title: "Coordinate", style: .plain, target: self, action: #selector(getPosition))
            self.navigationItem.rightBarButtonItem = rightBut
            self.navigationItem.rightBarButtonItem?.image = #imageLiteral(resourceName: "findMe")
        }
    }
    
    func getPosition() {
            let lat1 = Double(lat)
            let long1 = Double(long)
            let VC = self.storyboard?.instantiateViewController(withIdentifier: "map") as? MapViewController
            VC?.lat1 = lat1
            print(lat)
            VC?.lat2 = long1
            print(long)
            self.navigationController?.pushViewController(VC!, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
