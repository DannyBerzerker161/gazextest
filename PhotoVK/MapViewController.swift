import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    @IBOutlet weak var mapView: MKMapView!

    var lat1 : Double?
    var lat2 : Double?
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        let sourceloc = CLLocationCoordinate2D(latitude: lat1!, longitude: lat2!)
        let sourcePlacemark = MKPlacemark(coordinate: sourceloc, addressDictionary: nil)
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let sourceAnnot = MKPointAnnotation()
        sourceAnnot.title = "Photo Place!"
        if let location = sourcePlacemark.location {
            sourceAnnot.coordinate = location.coordinate
        }
        self.mapView.showAnnotations([sourceAnnot], animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
