
import SwiftyVK
import UIKit

var myPublicId : String = ""



class VKDelegateImp: VKDelegate {
    let appID = "5830447"
    let scope: Set<VK.Scope> = [.offline,.wall,.photos,.email]
    
    
    
    init() {
        VK.config.logToConsole = true
        VK.configure(withAppId: appID, delegate: self)
    }
    
    
    
    func vkWillAuthorize() -> Set<VK.Scope> {
        print("vkWillAutorize")
        return scope
    }
    
    
    
    func vkDidAuthorizeWith(parameters: Dictionary<String, String>) {
        print("vkDidAutorize")
        myPublicId = parameters["user_id"]!
        print("my Public Id = \(myPublicId)")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "TestVkDidAuthorize"), object: nil)
        
        if VK.state == .authorized {
            print("to navigationVC")
            
            let contr = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "navigationVC")
           UIApplication.shared.delegate?.window??.rootViewController = contr
            
            
        }
    }
    
    
    
    
    func vkAutorizationFailedWith(error: AuthError) {
        print("Autorization failed with error: \n\(error)")
        NotificationCenter.default.post(name: Notification.Name(rawValue: "TestVkDidNotAuthorize"), object: nil)
    }
    
    
    
    func vkDidUnauthorize() {
        print("vkDidUnAvtorized")
            print("to navigationVC")
            
            let contr = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginVC")
            UIApplication.shared.delegate?.window??.rootViewController = contr
            
            
        
    }
    
    
    
    func vkShouldUseTokenPath() -> String? {
        return nil
    }
    
    

    func vkWillPresentView() -> UIViewController {
        
       

      return UIApplication.shared.delegate!.window!!.rootViewController!
        
    }
    
}
