
import UIKit
import SwiftyVK

struct PhotoStruct {
    let photoID : String
    let text : String?
    let albumID : String
    let url_130 : String
    let url_max : String
    let lat : String
    let long : String
}

class photoAlbumTableViewController: UITableViewController, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate{
    
    var albumId : String = ""
    var allPhoto = [PhotoStruct]()
    let picker = UIImagePickerController()

    fileprivate var refreshController : UIRefreshControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("MY ALBUM ID = \(albumId)")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.tableFooterView = UIView()

        picker.delegate = self

        print("AlbumId = \(albumId)")
        getFotoFromAlbum(album: albumId)
        refresh()
    }
    
    func refresh() {
        self.refreshController = UIRefreshControl()
        self.refreshController.bounds = CGRect(x: 0, y: -20, width: refreshController.bounds.size.width, height: refreshController.bounds.size.height)
        self.refreshController.attributedTitle = NSAttributedString(string: "Потяните, чтобы обновить.")
        self.refreshController.addTarget(self, action: #selector(refresh(_:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(refreshController)
        
    }
    
    //MARK: - UPDATE DATA METHOD
    func refresh(_ sender: AnyObject) {
        allPhoto = [PhotoStruct]()
        getFotoFromAlbum(album: albumId)
        refreshController.endRefreshing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return allPhoto.count + (Int(albumId)! > 0 ? 1 : 0)
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    
    /*   override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let frame : CGRect = tableView.frame
        let userBtn : UIButton = UIButton(frame: CGRect(x: frame.size.width / 7, y: 0, width: 200, height: 35))
        userBtn.setTitle("Coordinate", for: .normal)
        userBtn.backgroundColor = UIColor.clear
        userBtn.titleLabel?.font = UIFont(name: "Menlo", size: 21)
        if allPhoto[section].lat == "" || allPhoto[section].long == ""  {
            userBtn.isHidden = false
        } else {
            var arr = [Double]()
            arr.append(Double(allPhoto[section].lat)!)
            arr.append(Double(allPhoto[section].long)!)
            let defaults = UserDefaults.standard
            defaults.set(arr, forKey: "coordinate")
            userBtn.addTarget(self, action: #selector(photoAlbumTableViewController.buttonTapped(sender:)), for: .touchUpInside)
        }
        let headerV : UIView = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        headerV.backgroundColor = UIColor.cyan
        headerV.addSubview(userBtn)
        return headerV
    } */
    
    func buttonTapped(sender: UIButton) {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "map") as? MapViewController
        let defaults = UserDefaults.standard
        let arr = defaults.value(forKey: "coordinate") as? [Double]
        VC?.lat1 = arr?[0]
        VC?.lat2 = arr?[1]
        self.navigationController?.pushViewController(VC!, animated: true)
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section != allPhoto.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "fotoCell", for: indexPath) as! FotoCell
            
            var activity =  UIActivityIndicatorView()
            activity.frame.size.width = 15
            activity.frame.size.height = 15
            activity.hidesWhenStopped = true
            activity.activityIndicatorViewStyle = .whiteLarge
            activity.color = UIColor.black
            activity.center.x = cell.photoImage.center.x
            activity.center.y = cell.photoImage.center.y
            cell.addSubview(activity)
            activity.startAnimating()
            
            let url = allPhoto[indexPath.section].url_130
            AsyncImageLoader.sharedLoader.imageForUrl(urlString: url, completionHandler: {(image: UIImage?, url: String) in
                activity.stopAnimating()
                activity.removeFromSuperview()
                cell.photoImage.image = image
            })
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "uploadCell", for: indexPath) as? UITableViewCell
            
            let lb : UILabel? = cell?.viewWithTag(3) as! UILabel?
            cell?.addSubview(lb!)
            return cell!

        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section != allPhoto.count {
            self.performSegue(withIdentifier: "FotoVC", sender: self)
        } else {
                let alert = UIAlertController(title: "Choose photo!", message: "", preferredStyle: .actionSheet)
            
                let camerBtn = UIAlertAction(title: "From Camera", style: .default) { (alert: UIAlertAction!) -> Void in
                    self.picker.allowsEditing = false
                    self.picker.sourceType = .camera
                    self.picker.cameraCaptureMode = .photo
                    self.picker.modalPresentationStyle = .fullScreen
                    self.present(self.picker,animated: true,completion: nil)
                }

                let libBtn = UIAlertAction(title: "From library", style: .default) { (alert: UIAlertAction!) -> Void in
                    self.picker.allowsEditing = false
                    self.picker.sourceType = .photoLibrary
                    self.present(self.picker,animated: true,completion: nil)
                }

                let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert: UIAlertAction!) -> Void in
                
                }
            
                alert.addAction(camerBtn)
                alert.addAction(libBtn)
                alert.addAction(cancel)
                self.present(alert, animated: true, completion: nil)
                }
        
    }
    
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section != allPhoto.count {
            return allPhoto[section].text == "" ? "No Text :(" : allPhoto[section].text }
        else {
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier == "FotoVC" {
            let vc = segue.destination as? FotoVC
            
            let row = self.tableView.indexPathForSelectedRow?.section
            let cell = self.tableView.cellForRow(at: self.tableView.indexPathForSelectedRow!) as! FotoCell
            let url = URL(string:  (allPhoto[row!].url_max as? String)!)
            let data = try? Data(contentsOf: url!)
            vc?.image = UIImage(data: data!)
            vc?.lat = allPhoto[row!].lat
            
            vc?.long = allPhoto[row!].long
            
        }

    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func getFotoFromAlbum ( album : String  ) {
        VK.API.Photos.get([VK.Arg.ownerId : myPublicId , .albumId : album]).send(onSuccess: { responce in
            print("On Succes Block")
            let items = responce["items"]
            for item in items {
                let lat = item.1["lat"].stringValue
                let long = item.1["long"].stringValue
                print("\n\n\n \(lat) --- \(long) \n\n\n")
                let url1 = item.1["photo_130"].stringValue
                let txt = item.1["text"].stringValue
                let url2 = item.1["photo_604"].stringValue
                let gg = PhotoStruct(photoID: "", text: txt, albumID: "", url_130: url1, url_max: url2, lat : lat, long : long)
                self.allPhoto.append(gg)
            }
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
            
        }, onError:{ error in print(error)
        }, onProgress: {done , total in print("send \(done) of \(total) bytes") })
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print("yeaaa")
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        let data = UIImagePNGRepresentation(chosenImage) as Data?
        let media  = Media(imageData: data!, type: .JPG)
        print("ALBUM ID = \(albumId)")
        
       let req = VK.API.Upload.Photo.toAlbum([media], albumId: albumId)
        let alert = UIAlertController(title: "Please wait", message: "Loading now", preferredStyle: .alert)
        req.send( onSuccess: { response in
            print("yeeeea + \(response)")
            alert.dismiss(animated: true, completion: nil)
        }, onError: { error in
            print("error")
        }, onProgress: { done, total in
            print(done, total)
        })
        dismiss(animated: true, completion: {
            self.present(alert, animated: true, completion: nil)
        })
    
    }
}
