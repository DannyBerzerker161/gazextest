
import UIKit
import SwiftyVK
import CoreData

struct AlbumVK {
     let albumTitle : String
    let albumId : String
    let thumb_scr : String
    let countFotoinAlbum : Int
    
}

var dataObj = [NSManagedObject]()

class AlbumsTableViewController: UITableViewController {
    
    
    var photoAlbum = 0
    var dictionaryAlbumIds = [String : String]()
    var allInfo = [AlbumVK]()

    override func viewDidLoad() {
        super.viewDidLoad()
        getAlbum()
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        if !Reachability.isConnectedToNetwork() {
            print("\n\n\n\n FETCH \n\n\n")
            fetch()
        } else {
            deleteEntity()
        }
    }
    
    private func fetch() {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appdelegate.managedObjectContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Album")
        do {
            let result = try managedContext.fetch(fetchRequest) as? [NSManagedObject]
            dataObj = result!
            
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }

    }

    func saveCnt(newDate : String, imgData : NSData) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        
        print("\n\n\n\n MUSTNT \n\n\n")
        let entity =  NSEntityDescription.entity(forEntityName: "Album",in: managedContext)
        let manObj = NSManagedObject(entity: entity!, insertInto: managedContext)
        manObj .setValue(newDate, forKey: "namelb")
        manObj .setValue(imgData, forKey: "photo")
        do {
            try managedContext.save()
            dataObj.append(manObj)
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

   
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if Reachability.isConnectedToNetwork() {
            return  allInfo.count
        } else {
            return dataObj.count
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if Reachability.isConnectedToNetwork() {
            return allInfo[section].albumTitle
        } else {
            let obj = dataObj[section]
            print("\n\n\n\n LABEL \n\n\n")
            return obj.value(forKey: "namelb") as! String?
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func deleteEntity() {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appdelegate.managedObjectContext
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName : "Album")
        do {
            let result = try managedContext.fetch(fetchReq) as? [NSManagedObject]
            for manageObject in result! {
                managedContext.delete(manageObject)
            }
        } catch {
            print("error")
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath) as! MyAlbumTableViewCell
        if Reachability.isConnectedToNetwork() {
            var activity =  UIActivityIndicatorView()
            activity.frame.size.width = 15
            activity.frame.size.height = 15
            activity.hidesWhenStopped = true
            activity.activityIndicatorViewStyle = .whiteLarge
            activity.color = UIColor.black
            activity.center.x = cell.thumbnail.center.x
            activity.center.y = cell.thumbnail.center.y
            cell.addSubview(activity)
            activity.startAnimating()
            let url = allInfo[indexPath.section].thumb_scr
            let lb = self.allInfo[indexPath.section].albumTitle
            AsyncImageLoader.sharedLoader.imageForUrl(urlString: url, completionHandler: {(image: UIImage?, url: String) in
                activity.stopAnimating()
                activity.removeFromSuperview()
                cell.thumbnail.image = image
                let imgData = NSData(data: UIImageJPEGRepresentation(image!, 1.0)!)
                let lb = self.allInfo[indexPath.section].albumTitle
                self.saveCnt(newDate: lb, imgData: imgData)
            })
        } else {
            print("\n\n\n\n `IMAGE \n\n\n")
            let obj = dataObj[indexPath.section]
            cell.thumbnail.image = UIImage(data: obj.value(forKey: "photo") as! Data, scale: 1.0)
        }
        
    
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.tableView.cellForRow(at: indexPath) as! MyAlbumTableViewCell
        self.performSegue(withIdentifier: "fotoAlbumTable", sender: self)
    }
    

       
    // MARK: - Navigation

    
    @IBAction func logOut(_ sender: UIBarButtonItem) {
        print("fff")
     VK.logOut()
        
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        print("prepareFOR segue")
        if segue.identifier == "fotoAlbumTable" {
            let vc = segue.destination as! photoAlbumTableViewController
            let row = self.tableView.indexPathForSelectedRow?.section
            let aa  : Int = (row)!
            let cell = self.tableView.cellForRow(at: self.tableView.indexPathForSelectedRow!) as! MyAlbumTableViewCell
            vc.albumId = allInfo[aa].albumId
            
            
            
        }
        
    }

    
    //MARK: VK get 
    
    func getAlbum (){
        VK.API.Photos.getAlbums([VK.Arg.ownerId : myPublicId  , .needCovers : "1"  , .needSystem : "1"]).send(onSuccess: {
                response in
            print("onSuccessBlock")
            print(response)
   
            print(response["items"][1]["title"])
            
            let items = response["items"]
            
            for item in items {
                let name : String = item.1["title"].stringValue
                let idAlbum = item.1["id"].stringValue
                let urlFoto = item.1["thumb_src"].stringValue
                let size = item.1["size"].intValue
                
                let album = AlbumVK(albumTitle: name, albumId: idAlbum, thumb_scr: urlFoto, countFotoinAlbum: size)
                self.allInfo.append(album)
                
            }
            
            DispatchQueue.main.async {
                 self.tableView.reloadData()
                print(self.allInfo)
            }
            
        },  onError: { error in
            print("ErrorBlock")
            print(error)  },
            onProgress: {progress in
                print( "ProgressBlock")
                print(progress) })
    }
    

}
