

import UIKit

class MyAlbumTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var thumbnail: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.clear
        thumbnail.layer.cornerRadius = thumbnail.frame.size.width / 1.3
        thumbnail.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
