

import UIKit

class FotoCell: UITableViewCell {

    @IBOutlet weak var  photoImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clear
        photoImage.layer.cornerRadius = photoImage.frame.size.width / 1.3
        photoImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
